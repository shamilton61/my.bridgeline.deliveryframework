﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;
using Bridgeline.FW.Global.ConfigHelper;
using Bridgeline.FW.Global.Security;
using Bridgeline.FW.Global.Utility;

namespace Bridgeline.DeliveryFramework
{

    /// <summary>
    /// Simple class uses the IDisposable interface mechanics to allow an easy method to switch context to the IntrinsicFrameworkAccount
    /// 
    /// Use a new instance of this class wrapped in a using construct.  It will swap to the intrinsic account constext on 
    /// instantiation and will revert to the original context upon disposal.
    /// </summary>
    public class FWPrincipalContext : IDisposable
    {
        IPrincipal currentPrincipal;


        /// <summary>
        /// Create a new instance of FWPrincipalContext.  In this constructor the context will be switched to the IntrinsicFrameworkAccount.
        /// 
        /// Please ensure that you are appropriatelly using this inside of a using construct in order to revert the context on disposal.
        /// </summary>
        public FWPrincipalContext()
        {
            currentPrincipal = Thread.CurrentPrincipal;

            try
            {
                Guid intrinsicId = Util.FrameworkSection.IntrinsicFrameworkAccount;
                Guid productId = FWAppSettings.GetGuid("CMSProductId");

                var user = Membership.GetUser(intrinsicId);
                FWIdentity iappsIdentity = new FWIdentity(user);
                FWPrincipal iappsPrincipal = new FWPrincipal(productId, new Guid(Membership.ApplicationName), iappsIdentity);

                HttpContext.Current.User = iappsPrincipal;
                Thread.CurrentPrincipal = iappsPrincipal;
            }
            catch (Exception)
            {
                HttpContext.Current.User = currentPrincipal;
                Thread.CurrentPrincipal = currentPrincipal;

                throw;
            }
        }


        /// <summary>
        /// Dispose the FWPrincipalContext object, reverting the account context to the user context in use at the time of creation of this object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Dispose the FWPrincipalContext object, reverting the account context to the user context in use at the time of creation of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            HttpContext.Current.User = currentPrincipal;
            Thread.CurrentPrincipal = currentPrincipal;
        }
    }
}
