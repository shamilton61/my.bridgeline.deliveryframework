﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Bridgeline.DeliveryFramework.Controls
{
    /// <summary>
    /// Simple control class to display a formatted Date/Time string where placed in the markup
    /// </summary>
    public class DateTimeDisplay : Control
    {
        private string formatString = "MM/dd/yy H:mm:ss";

        /// <summary>
        /// Provide a FormatString to passed to the DateTime.ToString method
        /// 
        /// Follows convention of
        /// http://msdn.microsoft.com/en-us/library/zdtaw1bw(v=vs.110).aspx
        /// </summary>
        public string FormatString { get { return formatString; } set { formatString = value; } }
        
        /// <summary>
        /// Override of the Render method inherited from Control
        /// 
        /// This method will write the results to the HtmlTextWriter used in the page lifecycle
        /// Uses valud from FormatString to format the output
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            writer.Write(DateTime.Now.ToString(formatString));
        }
    }
}
